import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
private baseUrl = 'https://www.empulseit.com/LinkedgeAPIDemo/api/';
  
  // private baseUrl='http://localhost:60237/api/';

   //Immutable set of Http headers, with lazy parsing.
 private _headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(public http: HttpClient) {    
  }

  get(url) {
    return this
      .http
      .get(this.baseUrl + url);
  }

  post(url: string, data: any) {
    return this
      .http
      .post(this.baseUrl + url, data);
  }

  put(url: string, data: any) {
    return this
      .http
      .put(this.baseUrl + url, JSON.stringify(data));
  }

  delete(url: string, data?: any) {
    return this
      .http
      .post(this.baseUrl + url, data);
  }
}
