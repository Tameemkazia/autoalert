import {FormGroup, FormArray } from '@angular/forms'

export function CompareQuantityReceive(){
    return (formGroup:FormGroup)=>{
        let isSumExceed:boolean=false;
        let expQty = formGroup.controls['expQty'].value==null?0:formGroup.controls['expQty'].value;
        //let expecUom=formGroup.controls['expUom'].value==null?'':formGroup.controls['expUom'].value;
        let adjQty = formGroup.controls['shortageAdjust'].value==null?0:formGroup.controls['shortageAdjust'].value;
        //let adjUom=formGroup.controls['uom'].value==null?'':formGroup.controls['uom'].value;//this.receive.uom.value;
        let damQty = formGroup.controls['damageAdjust'].value==null?0:formGroup.controls['damageAdjust'].value;//this.receive.damageAdjust.value;
        //let damUom= formGroup.controls['damageuom'].value==null?'':formGroup.controls['damageuom'].value;//this.receive.damageuom.value;
        //let convValue = formGroup.controls['convention'].value==null?0:formGroup.controls['convention'].value;//this.conventionValue;
        //console.log(convValue);
        //let isUomDiff:boolean=false;

        // if((adjUom!='' && expecUom!=adjUom) || 
        // (damUom!='' && expecUom!=damUom) ||
        // (damUom!='' && adjUom!='' && adjUom!=damUom)){
        //   isUomDiff=true;
        // }
        // if(isUomDiff && convValue>0){
        //   expQty=expecUom != "PCS"?expQty*convValue:expQty;
        //  if(adjUom!=''){ adjQty=adjUom != "PCS"?adjQty*convValue:adjQty;}
        //  if(damUom!=''){  damQty=damUom != "PCS"?damQty*convValue:damQty;}
        // }
        //if(adjQty+damQty>expQty){isSumExceed=true}

        // if(adjUom!='' && adjQty>0 && (adjQty>expQty || isSumExceed==true)){
        //     formGroup.controls['shortageAdjust'].setErrors({qtyExceed:true});
        // }else{
        //     formGroup.controls['shortageAdjust'].setErrors(null);
        // }
        if(adjQty<0){
            formGroup.controls['shortageAdjust'].setErrors({minError:true});
        }
        else{
            formGroup.controls['shortageAdjust'].setErrors(null);
        }
        if(damQty<0){
            formGroup.controls['damageAdjust'].setErrors({minError:true});
        }
        else{
            formGroup.controls['damageAdjust'].setErrors(null);
        }
        // if(damUom!='' && damQty>0 && (damQty>expQty || isSumExceed==true)){
        //     formGroup.controls['damageAdjust'].setErrors({qtyExceed:true});
        // }else{
        //     formGroup.controls['damageAdjust'].setErrors(null);
        // }
    }
}