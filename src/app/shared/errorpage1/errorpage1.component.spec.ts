import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Errorpage1Component } from './errorpage1.component';

describe('Errorpage1Component', () => {
  let component: Errorpage1Component;
  let fixture: ComponentFixture<Errorpage1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Errorpage1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Errorpage1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
