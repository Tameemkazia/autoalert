import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-company-creation',
  templateUrl: './company-creation.component.html',
  styleUrls: ['./company-creation.component.css']
})
export class CompanyCreationComponent implements OnInit {
  companyForm: FormGroup;
  submitted: boolean = false;
  userObj: any;
  paramValue: any;
  editCompany: boolean = false;
  editCompanyId: any;
  companyDetailsData: any;
  selectedFilename: string = "Company Logo";
  image: any;

  constructor(private spinner:NgxSpinnerService, private router: Router, private route: ActivatedRoute,
    private formBuilder: FormBuilder, private restApi: ApiService) {
      this.companyForm = this.formBuilder.group({
        org_Name: ['', Validators.required],
        address: ['', Validators.required],
        phone_No: ['', Validators.required],
        mail_Id: ['', Validators.required],
        logo_Path: ['test', Validators.required]
      });
     }

  ngOnInit() {
    this.route.queryParams.subscribe(queryParams => {
      this.paramValue = queryParams;
      debugger;
      if(this.paramValue.param !== undefined){
        this.editCompany = true;
        this.editCompanyId = this.paramValue.param;
        this.getCompanyDetails(this.paramValue.param);
      }
  });
  }

  OnbuttonClickClose() {
    this.router.navigate(['/layout/company']);
  }

  getCompanyDetails(_id){
    this.spinner.show();
    this.restApi.getCompanyById(_id).subscribe(_res=>{
      this.companyDetailsData = _res;
      this.spinner.hide();
      this.companyForm.patchValue(this.companyDetailsData);
    })
  }
  
  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    if (file !== undefined) {
      var myReader: FileReader = new FileReader();
      this.selectedFilename = file.name;
      // this.selectedFilename = (file === undefined) ? "Company Logo" : file.name;
      myReader.onloadend = (e) => {
        this.image = myReader.result;
        this.companyForm.value.logo_Path = this.image;
        debugger;
      }
      myReader.readAsDataURL(file);
    } else {
      this.selectedFilename = "Company Logo";
      this.image = "";
    }
  }

  get f() { return this.companyForm.controls; }

  submit() {

    this.submitted = true;

    if (this.companyForm.invalid) {
      return
    }
    this.spinner.show();
    this.userObj = JSON.parse(localStorage.getItem('user'));
    this.companyForm.value.logo_Path = this.image;
    debugger;
    if (this.editCompany === false) {
      this.companyForm.value.contact_Person = this.userObj.user_name;
      this.companyForm.value.created_By = this.userObj.user_id;
      this.restApi.createCompany(this.companyForm.value).subscribe(_res => {
        this.router.navigate(['/layout/company']);
        this.spinner.hide();
      })
    }
    else {
      
      this.companyForm.value.is_deleted = "0";
      this.companyForm.value.created_By = this.userObj.user_id;
      this.companyForm.value.org_id = this.editCompanyId;
      debugger;
      this.restApi.updateCompany(this.companyForm.value).subscribe(_res => {
        this.router.navigate(['/layout/company']);
        this.spinner.hide();
      })
    }

  }
}