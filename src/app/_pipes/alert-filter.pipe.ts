import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'alertFilter'
})
export class AlertFilterPipe implements PipeTransform {

  transform(items: any[], nameSearch: string): any {
    debugger;
    if(nameSearch!= undefined && items && items.length > 0){
      items = items.filter(item => item.Assigned_To.toLowerCase().includes(nameSearch.toLowerCase()));
      console.log('items',items);
      return items;
    } else if(items.length == 0 ){
      return items;
  }else{
      return items;
  }
  
  }

}

