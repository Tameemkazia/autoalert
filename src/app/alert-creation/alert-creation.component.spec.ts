import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertCreationComponent } from './alert-creation.component';

describe('AlertCreationComponent', () => {
  let component: AlertCreationComponent;
  let fixture: ComponentFixture<AlertCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
