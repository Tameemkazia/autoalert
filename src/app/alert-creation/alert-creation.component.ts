import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { Directive, EventEmitter, Input, Output, QueryList, ViewChildren } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToasterService } from '../helper/toaster.service';

@Component({
  selector: 'app-alert-creation',
  templateUrl: './alert-creation.component.html',
  styleUrls: ['./alert-creation.component.css']
})
export class AlertCreationComponent implements OnInit {
  [x: string]: any;
  alertcreationdata: FormGroup;
  http: any;
  departmentdata: any = [{ departmentdata: {} }];
  userObj: any;
  usersoforg: any = [{ usersoforg: {} }];
  alertDetails: any;
  departmentdatasorted: any = [{ departmentdatasorted: {} }];
  paramValue: any;
  editAlert: boolean = false;
  editAlertId: any;
  alertDetailsData: any;
  allApis: any;
  submitted: boolean = false;
  selectedFilename: any = "Upload Doc";
  image: any;

  constructor(private toastr:ToasterService, private spinner: NgxSpinnerService, private router: Router,
    private formBuilder: FormBuilder, private restApi: ApiService, private route: ActivatedRoute) {
      this.userObj = JSON.parse(localStorage.getItem('user'));
    console.log(this.userObj.org_id);
    console.log(this.userObj.first);
    this.alertcreationdata = this.formBuilder.group({
      "org_id": [this.userObj.org_id],
      "role": [this.userObj.role],
      "alert_num": ['', Validators.required],
      "alert_Name": ['', Validators.required],
      "alert_createddate": ['2020-01-14'],
      "dept_id": ['', Validators.required],
      // "assignedTo": ['', Validators.required],
      "addnl_spdays": ['', Validators.compose([Validators.required, Validators.pattern("[0-9]+")])],
      "supp_uom": ['',Validators.required],
      "role_id":[''],
      "priority": [''],
      "dev_api_id": ['',Validators.required],
      // "api_callfreq1": ['', Validators.compose([Validators.required, Validators.pattern("[0-9]+")])],
      "api_callfreq": ['', Validators.compose([Validators.required, Validators.pattern("[0-9]+")])],
      "apifreq_uom": ['',Validators.required],
      "doc":[''],
      "notes": [''],
      "rules": [''],
      "deviation_value": ['', Validators.compose([Validators.required, Validators.pattern("[0-9]+"), Validators.min(0), Validators.max(100)])],
      "deviation_target":[''],
      "created_by": [this.userObj.user_id],
      "created_date": [new Date().toISOString()],
      "modified_by": [this.userObj.user_id],
      "modified_date": [new Date().toISOString()],
      "is_deleted": ['0'],
      "diagrange": [''],
      "frequency": [''],
      "trigger_point": [''],
      "histcycle_time": [''],
      "params_target": [''],
      "actual_params": [''],
      "dateTime": [''],
      "description": [''],
      "ack_duration": [''],
      "ack_time_uom": [''],
      "ack_assigned_To": [''],
      "diag_duration": [''],
      "diag_uom": [''],
      "diag_assigned_to": [''],
      "action_duration": [''],
      "action_uom": [''],
      "action_assigned_to": [''],
      "close_duration": [''],
      "close_uom": [''],
      "close_assigned_to": [''],
      "observe_duration": [''],
      "observe_uom": [''],
      "observe_assigned_to": [''],
      "comu_req": ['1'],
      "sms_req": [''],
      "sms_nos":[''],
      "email_req":[''],
      "email_ids":[''],
      "whatsapp_req":[''],
      "whatsapp_nos":[''],
      "sms_message":[''],
      "email_message":[''],
      "wapp_message":['']
    });
     }

  ngOnInit() {
this.spinner.show();
    this.route.queryParams.subscribe(queryParams => {
      this.paramValue = queryParams;
      debugger;
      if(this.paramValue.param !== undefined){
        this.editAlert = true;
        this.editAlertId = this.paramValue.param;
        this.getAlertDetails(this.paramValue.param);
      }else{
        this.getAllApis();
      }
  });


  
    

    this.restApi.getDepartmentByOrgId().subscribe((departmentdatajson) => {
      //console.log(departmentdatajson);
      this.departmentdatasorted = departmentdatajson;
      
      // for(let i =0;i <Object.keys(this.departmentdata).length;i++){
      //   if(this.departmentdata[i].org_id == this.userObj.org_id){
      //    this.departmentdatasorted.push(this.departmentdata[i]);
      //   }
      // }
    });

    this.restApi.getUsersByOrgId().subscribe((usersdatajson) => {
      console.log(usersdatajson);
      this.usersoforg = usersdatajson;
    });
    this.spinner.hide();
  }


  changeListener($event): void {
    setTimeout(() => {
      this.readThis($event.target);
    }, 100);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    if (file !== undefined) {
      var myReader: FileReader = new FileReader();
      this.selectedFilename = file.name;
      // this.selectedFilename = (file === undefined) ? "Company Logo" : file.name;
      myReader.onloadend = (e) => {
        this.image = myReader.result;
        this.alertcreationdata.value.doc = this.image;
        debugger;
      }
      myReader.readAsDataURL(file);
    } else {
      this.selectedFilename = "Upload Doc";
      this.image = "";
    }
  }

  getAlertDetails(_id) {
    this.spinner.show();
    this.restApi.getAlertDetails(_id).subscribe(_res=>{
      this.alertDetails = _res;
      this.spinner.hide();
      this.getAllApis();
      this.alertcreationdata.patchValue(this.alertDetails);
      // this.alertDetailsData.controls["org_id"].setValue(this.alertDetails.org_id);
      // this.alertDetailsData.controls["role"].setValue(this.alertDetails.role_id);
      // this.alertDetailsData.controls["alert_num"].setValue(this.alertDetails.alert_num);
      // this.alertDetailsData.controls["alert_Name"].setValue(this.alertDetails.alert_Name);
      // this.alertDetailsData.controls["alert_createddate"].setValue(this.alertDetails.alert_createddate);
      // this.alertDetailsData.controls["dept_id"].setValue(this.alertDetails.dept_id
      // this.alertDetailsData.controls["assignedTo"].setValue(this.alertDetails
      // this.alertDetailsData.controls["role_id"].setValue(this.alertDetails
      // this.alertDetailsData.controls["addnl_spdays"].setValue(this.alertDetails
      // this.alertDetailsData.controls["addnl_spdays_drop"].setValue(this.alertDetails
      // this.alertDetailsData.controls["priority"].setValue(this.alertDetails
      // this.alertDetailsData.controls["dev_api_id"].setValue(this.alertDetails
      // this.alertDetailsData.controls["api_callfreq1"].setValue(this.alertDetails
      // this.alertDetailsData.controls["api_callfreq2"].setValue(this.alertDetails
      // this.alertDetailsData.controls["notes"].setValue(this.alertDetails
      // this.alertDetailsData.controls["rules"].setValue(this.alertDetails
      // this.alertDetailsData.controls["deviation_value"].setValue(this.alertDetails
      // this.alertDetailsData.controls["created_by"].setValue(this.alertDetails
      // this.alertDetailsData.controls["created_date"].setValue(this.alertDetails
      // this.alertDetailsData.controls["modified_by"].setValue(this.alertDetails
      // this.alertDetailsData.controls["modified_date"].setValue(this.alertDetails
      // this.alertDetailsData.controls["is_deleted"].setValue(this.alertDetails
      // this.alertDetailsData.controls["diagrange"].setValue(this.alertDetails
      // this.alertDetailsData.controls["frequency"].setValue(this.alertDetails
      // this.alertDetailsData.controls["trigger_point"].setValue(this.alertDetails
      // this.alertDetailsData.controls["histcycle_time"].setValue(this.alertDetails
      // this.alertDetailsData.controls["params_target"].setValue(this.alertDetails
      // this.alertDetailsData.controls["actual_params"].setValue(this.alertDetails
      // this.alertDetailsData.controls["dateTime"].setValue(this.alertDetails
      // this.alertDetailsData.controls["desc"].setValue(this.alertDetails
      // this.alertDetailsData.controls["ack_duration"].setValue(this.alertDetails
      // this.alertDetailsData.controls["ack_time_uom"].setValue(this.alertDetails
      // this.alertDetailsData.controls["ack_assigned_To"].setValue(this.alertDetails
      // this.alertDetailsData.controls["diag_duration"].setValue(this.alertDetails
      // this.alertDetailsData.controls["diag_uom"].setValue(this.alertDetails
      // this.alertDetailsData.controls["diag_assigned_to"].setValue(this.alertDetails
      // this.alertDetailsData.controls["action_duration"].setValue(this.alertDetails
      // this.alertDetailsData.controls["action_uom"].setValue(this.alertDetails
      // this.alertDetailsData.controls["action_assigned_to"].setValue(this.alertDetails
      // this.alertDetailsData.controls["close_duration"].setValue(this.alertDetails
      // this.alertDetailsData.controls["close_uom"].setValue(this.alertDetails
      // this.alertDetailsData.controls["close_assigned_to"].setValue(this.alertDetails
      // this.alertDetailsData.controls["observe_duration"].setValue(this.alertDetails
      // this.alertDetailsData.controls["observe_uom"].setValue(this.alertDetails
      // this.alertDetailsData.controls["observe_assigned_to"].setValue(this.alertDetails
    })
  }

  OnbuttonClickClose() {
    this.router.navigate(['/layout/alert']);
  }

  get f(){ return this.alertcreationdata.controls; }

  onsubmit() {
     this.submitted = true;
     debugger;
    if (this.alertcreationdata.invalid) {
      return
    }
   
    console.log(this.alertcreationdata.value);
    this.alertcreationdata.value.doc = this.image;
    if(this.editAlert === false){
      this.restApi
      .createAlert(this.alertcreationdata.value)
      .subscribe(_res => {
        debugger;
        this.router.navigate(['/layout/alert']);
      });
    }else{
      //this.alertcreationdata.addControl('alert_Id', this.editAlertId);
      this.alertcreationdata.value.alert_Id = this.editAlertId;
      this.restApi
      .updateAlert(this.alertcreationdata.value)
      .subscribe(_resp => {
        this.router.navigate(['/layout/alert']);
      });
    }
   
  }

  checkAlertExists(){
    this.restApi.alertExists(this.userObj.org_id, this.alertcreationdata.value.dev_api_id).subscribe(_res=>{
     var result = _res;
      if(result === 400){
        this.onsubmit();
      }else{
        this.toastr.info("Already alert create with the selected API");
      }
    })
  } 

  getAllApis(){
    this.spinner.show();
    this.restApi.getAllApis().subscribe(_res=>{
      this.spinner.hide();
      this.allApis = _res['apis'];
    })
  }

}