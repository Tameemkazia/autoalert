import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatInputModule, MatButtonModule, MatSelectModule, MatIconModule,MatTableModule, MatCard, MatToolbarModule, MatNativeDateModule,  } from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatCardModule} from '@angular/material/card';
import {MatRadioModule} from '@angular/material/radio';
import {MatTreeModule} from '@angular/material/tree';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
// ngx-bootstrap functionality
import { SharedBootstrapModule } from './shared/shared-bootstrap.module';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ng6-toastr-notifications';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule, RadioButtonModule, CheckboxModule } from 'primeng/primeng';
import { AlertComponent } from './alert/alert.component';
import { AlertCreationComponent } from './alert-creation/alert-creation.component';
import { ApiMappingComponent } from './api-mapping/api-mapping.component';
import { ApiMappingCreationComponent } from './api-mapping-creation/api-mapping-creation.component';
import { CompanyComponent } from './company/company.component';
import { CompanyCreationComponent } from './company-creation/company-creation.component';
import { ScrollDispatchModule } from '@angular/cdk/scrolling'; 
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LoginnewComponent } from './loginnew/loginnew.component';
import { OrderModule } from 'ngx-order-pipe';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import { BnNgIdleService } from 'bn-ng-idle';
import { AuthGuard } from './_guards/auth.guard';
import { MomentModule, DateFormatPipe, DifferencePipe } from 'ngx-moment';
import { ImagePreloadDirective } from './directives/img-preload';
import { AlertFilterPipe } from './_pipes/alert-filter.pipe';
import { NgxSpinnerModule } from "ngx-spinner";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    UserComponent,
    AlertComponent,
    AlertCreationComponent,
    ApiMappingComponent,
    ApiMappingCreationComponent,
    CompanyComponent,
    CompanyCreationComponent,
    LoginnewComponent,
    ImagePreloadDirective,
    AlertFilterPipe
  ],
  imports: [
    NgbModule,
    BrowserModule,
    HttpClientModule,
    TreeModule,
    TreeTableModule,
    RadioButtonModule,
    CheckboxModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DataTablesModule,
    SharedBootstrapModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule, 
    MatButtonModule, 
    MatSelectModule, 
    MatIconModule,
    MatTableModule,
    MatCardModule, 
    MatButtonModule,
   MatToolbarModule,
   MatDatepickerModule,
   MatNativeDateModule,
    MatRadioModule,
    MatCheckboxModule,MatTreeModule,
    CdkTreeModule,
    ScrollDispatchModule,
    DragDropModule,
    OrderModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    NgxPaginationModule,
    MomentModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot([
      {
        path: '', component: LoginComponent
      },
      { path: 'login', component: LoginComponent },
      {
        path: 'layout', component: LayoutComponent, canActivate: [AuthGuard],
        children: [
          { path: 'user', component: UserComponent, canActivate: [AuthGuard] },
          { path: 'alert', component: AlertComponent, canActivate: [AuthGuard] },
          { path: 'alertcreation', component: AlertCreationComponent, canActivate: [AuthGuard] },
          { path: 'apimapping', component: ApiMappingComponent, canActivate: [AuthGuard] },
          { path: 'apimappingcreation', component: ApiMappingCreationComponent, canActivate: [AuthGuard] },
          { path: 'company', component: CompanyComponent, canActivate: [AuthGuard] },
          { path: 'companycreation', component: CompanyCreationComponent, canActivate: [AuthGuard] },
        ]
      },

      // { path: '**', redirectTo: '/login', pathMatch: 'full' },
    ]),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot()
  ],
  providers: [BnNgIdleService, DateFormatPipe, DifferencePipe],
  bootstrap: [AppComponent],
  exports: [ImagePreloadDirective]
})
export class AppModule { }
