import { Component } from '@angular/core';
import { BnNgIdleService } from 'bn-ng-idle';
import { AuthenticationService } from './authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CriderDataMask';
  constructor(private bnIdle: BnNgIdleService, private authenticationService: AuthenticationService) { // initiate it in your component constructor
    console.log('App Component');
    this.bnIdle.startWatching(300).subscribe((res) => {
      if(res) {
        this.authenticationService.logout();
      }
    })
  }
}
