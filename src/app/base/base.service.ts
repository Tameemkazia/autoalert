import {HelperService} from '../helper/helper.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//import { HelperService } from 'src/app/helper/helper.service';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  
private baseUrl = 'https://www.empulseit.com/CriderDataMaskAPI/api/';
 //private baseUrl = 'http://localhost:52908/api/';
 // private baseUrl = 'http://10.1.1.16/CriderDataMaskAPI/api/';
  
private loginUrl = 'http://www.empulseit.com/GRPAPI/api/'
 private _headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(public http: HttpClient) {    
  }

  get(url) {
    return this
      .http
      .get(this.baseUrl + url);
  }
  post(url: string, data: any) {
    return this
      .http
      .post(this.baseUrl + url, data);
  }

  //login
  postWithHeader(url: string, data: any,header:any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Authorization':'Basic ' + btoa(header)
      })
    };
    return this
      .http
      .post(this.loginUrl + url, data,httpOptions);
  }

  put(url: string, data: any) {
    return this
      .http
      .put(this.baseUrl + url, JSON.stringify(data));
  }

  delete(url: string, data?: any) {
    return this
      .http
      .post(this.baseUrl + url, data);
  }
}
