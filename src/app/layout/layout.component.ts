import { Component, OnInit, Input, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ApiService } from '../api.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.css'],
    animations: [
        trigger('animation', [
            state('hidden', style({
                height: '0',
                overflow: 'hidden',
                maxHeight: '0',
                paddingTop: '0',
                paddingBottom: '0',
                marginTop: '0',
                marginBottom: '0',
                opacity: '0',
            })),
            state('void', style({
                height: '0',
                overflow: 'hidden',
                maxHeight: '0',
                paddingTop: '0',
                paddingBottom: '0',
                marginTop: '0',
                marginBottom: '0',
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible <=> hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('void => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('void => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class LayoutComponent implements OnInit {
    paramValue: any;
    userObj: any;
    logoPath: any;
    departmentdata: any = [{ departmentdata: {} }];
    menuActive: boolean;

    activeMenuId: string;

    darkDemoStyle: HTMLStyleElement;

    routes: Array<string> = [];

    filteredRoutes: Array<string> = [];

    searchText: string;
    status: number;

    constructor(private router: Router, private restApi: ApiService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.userObj = JSON.parse(localStorage.getItem('user'));
        console.log(this.userObj);
        console.log(this.userObj.org_id);
        console.log(this.userObj.first_name);
        this.route.queryParams.subscribe(queryParams => {
            this.paramValue = queryParams;
            // this.router.navigate(['/layout/user'], { queryParams: {param: queryParams.param, id: queryParams.param2}});
            debugger;
        });

        this.logoPath = "./assets/logos/" + this.userObj.org_id + "/logo.png";
        console.log(this.logoPath);

        let routes = this.router.config;
        for (let route of routes) {
            if (route.path && route.path !== "datagrid" && route.path !== "datalist" && route.path !== "datascroller" && route.path !== "growl")
                this.routes.push(route.path.charAt(0).toUpperCase() + route.path.substr(1));
        }

        this.restApi.getDepartmentByOrgId().subscribe((departmentdatajson) => {
            console.log('departmentdatajson', departmentdatajson);
            this.departmentdata = departmentdatajson;
        });


    }

    onAnimationStart(event) {
        switch (event.toState) {
            case 'visible':
                event.element.style.display = 'block';
                break;
        }
    }
    onAnimationDone(event) {
        switch (event.toState) {
            case 'hidden':
                event.element.style.display = 'none';
                break;

            case 'void':
                event.element.style.display = 'none';
                break;
        }
    }
    toggler(deptid: number) {
        this.status = 0;
        console.log(deptid);
        this.restApi.getMyDeptAlerts(this.status, deptid).subscribe((data) => {
            console.log(data);
            // this.alertdata = data;
        });
    }

    toggle(id: string) {
        this.activeMenuId = (this.activeMenuId === id ? null : id);
        this.status = 0;
        //  console.log(deptid);
        //   this.restApi.getMyDeptAlerts(status,deptid).subscribe((data)=>{
        // console.log(data);
    }
    // onKeydown(event: KeyboardEvent,id:string) {
    //     if (event.which === 32 || event.which === 13) {
    //         this.toggle(id);
    //         event.preventDefault();
    //     }
    // }

    selectRoute(routeName) {
        this.router.navigate(['/' + routeName.toLowerCase()]);
        this.filteredRoutes = [];
        this.searchText = "";
    }

    filterRoutes(event) {
        let query = event.query;
        this.filteredRoutes = this.routes.filter(route => {
            return route.toLowerCase().includes(query.toLowerCase());
        });
    }

    // changeTheme(event: Event, theme: string, dark: boolean) {
    //     let themeLink: HTMLLinkElement = <HTMLLinkElement> document.getElementById('theme-css');
    //     themeLink.href = 'assets/components/themes/' + theme + '/theme.css';

    //     if (dark) {
    //         if (!this.darkDemoStyle) {
    //             this.darkDemoStyle = document.createElement('style');
    //             this.darkDemoStyle.type = 'text/css';
    //             this.darkDemoStyle.innerHTML = '.implementation { background-color: #3f3f3f; color: #dedede} .implementation > h3, .implementation > h4{ color: #dedede}';
    //             document.body.appendChild(this.darkDemoStyle);
    //         }
    //     }
    //     else if(this.darkDemoStyle) {
    //         document.body.removeChild(this.darkDemoStyle);
    //         this.darkDemoStyle = null;
    //     }

    //     event.preventDefault();
    // }

    onMenuButtonClick(event: Event) {
        this.menuActive = !this.menuActive;
        event.preventDefault();
    }

    showFunction() {
        document.getElementById("layout-sidebar").style.display = "block"
        document.getElementById("hideSideebar").style.display = "block"
        document.getElementById("showSideebar").style.display = "none"
        document.getElementById("topBar_Menu").style.position = "relative"
        document.getElementById("topBar_Menu").style.top = "-92px"
    }

    hideFunction() {
        document.getElementById("layout-sidebar").style.display = "none"
        document.getElementById("hideSideebar").style.display = "none"
        document.getElementById("showSideebar").style.display = "block"
        document.getElementById("showSideebar").style.position = "relative"
        document.getElementById("showSideebar").style.left = "263px"
        document.getElementById("showSideebar").style.top = "-45px"
    }

}
