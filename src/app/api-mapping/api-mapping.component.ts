import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { retry, catchError, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-api-mapping',
  templateUrl: './api-mapping.component.html',
  styleUrls: ['./api-mapping.component.css']
})
export class ApiMappingComponent implements OnInit {
  allApis: any;
  userObj: any;
  @ViewChild(DataTableDirective, {static: false})
  datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  
  constructor(private spinner: NgxSpinnerService, private router: Router, private restApi: ApiService, private http: HttpClient) { }

  ngOnInit() {
    this.getAllApis();
  }



  OnbuttonClick() {
    this.router.navigate(['/layout/apimappingcreation']);
  }

  getAllApis(){
    this.spinner.show();
    this.userObj = JSON.parse(localStorage.getItem('user'));
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[ 0, "asc" ]],
    };

    this.http.get(this.restApi.apiURL+'/api/alert/getAllApi/'+this.userObj.org_id)
    .pipe(map(this.extractData))
      .subscribe(_res => {
        this.spinner.hide();
        this.allApis = _res;
        this.dtTrigger.next();
      });

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  private extractData(res: Response) {
    const body = res['apis'];
    debugger;
    return body || {};
  }

  removeApi(_data){
    this.spinner.show();
    this.userObj = JSON.parse(localStorage.getItem('user'));
    _data['is_deleted'] = "1";
    _data['created_By'] = this.userObj.user_id;
    this.restApi.updateApi(_data).subscribe(_res=>{
      this.spinner.hide();
      this.getAllApis();
    })
  }
}
