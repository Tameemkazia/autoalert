import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ApiService } from '../api.service';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { retry, catchError, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy {
  alertcreateddata :any;
  userObj: any;
  @ViewChild(DataTableDirective, {static: false})
  datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private spinner: NgxSpinnerService, private router: Router,private restApi: ApiService, private http: HttpClient) {
    
   }

  ngOnInit() {
    this.spinner.show();
    this.userObj = JSON.parse(localStorage.getItem('user'));
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[ 0, "asc" ]],
    };
    this.http.get(this.restApi.apiURL+'/api/alert/getAlertsByOrgId/'+this.userObj.org_id)
    .pipe(map(this.extractData))
      .subscribe(persons => {
        this.spinner.hide();
        this.alertcreateddata = persons;
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      });
     
      //this.getAlerts();
    
     //  this.dtOptions = this.http.get('http://localhost:8085/AutoAlert/api/alert/getAlertsByOrgId/2')
    //   .toPromise()
    //   .then((response) => response)
    //   .catch(this.handleError);

    //  this.dtOptions = {
    //   ajax: 'http://localhost:8085/AutoAlert/api/alert/getAlertsByOrgId/2',
    //   columns: [{
    //     title: 'Alert Number',
    //     data: 'alert_num'
    //   },
    //   {
    //     title: 'Alert Name',
    //     data: 'alert_Name'
    //   },
    //   {
    //     title: 'Created Date',
    //     data: 'alert_createddate'
    //   },
    //   {
    //     title: 'Department',
    //     data: 'dept_Id'
    //   },
    //   {
    //     title: 'Active Till',
    //     data: 'addnl_spdays'
    //   },
    //   {
    //     title: 'Priority',
    //     data: 'priority'
    //   },
    //   {
    //     title: 'API',
    //     data: 'api_callfreq'
    //   }
    // ]
    // };
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  private extractData(res: Response) {
    const body = res['orgAlerts'];
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  // ngAfterViewInit(): void {
  //   this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //     dtInstance.columns().every(function () {
  //       const that = this;
  //       $('input', this.footer()).on('keyup change', function () {
  //         if (that.search() !== this['value']) {
  //           that
  //             .search(this['value'])
  //             .draw();
  //         }
  //       });
  //     });
  //   });
  // }

  getAlerts(){
    this.spinner.show();
    this.restApi.getalertcreatedlist(this.userObj.org_id).subscribe((data)=>{
      this.alertcreateddata = data;
      this.spinner.hide();
    }); 
  }


  
  removeAlert(_id){
    this.spinner.show();
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

       this.restApi.deleteAlert(_id).subscribe(_res=>{
        Swal.fire(
          'Deleted!',
          'Your imaginary file has been deleted.',
          'success'
        )
        this.spinner.hide();
       })
       this.getAlerts();
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
        this.spinner.hide();
      }
    })
  }

  OnbuttonClick() {
    this.router.navigate(['/layout/alertcreation']);
  }

}
