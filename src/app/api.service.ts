import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  userObj: any;
  API_KEY = '';
  //apiURL = 'http://97.74.237.104:8085/AutoAlert';
  //apiURL = 'http://97.74.237.104:8088/AutoAlert';
  apiURL = 'http://localhost:8085/AutoAlert';
  private currentUserSubject: BehaviorSubject<object>;
  public currentUser: Observable<object>;
  constructor(private httpClient: HttpClient) {
    // this.currentUserSubject = new BehaviorSubject<object>(JSON.parse(localStorage.getItem('currentUser')));
    // this.currentUser = this.currentUserSubject.asObservable();
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
      //'Access-Control-Allow-Origin' 'http://localhost:8080' always;
    })
  }

  getmyAlert(status): Observable<object> {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    console.log(this.userObj.org_id);
    console.log(this.userObj.first_name);
    console.log(this.userObj.org_id + ':' + this.userObj.first_name + ':' + status);

    return this.httpClient.get<object>(this.apiURL + '/api/alert/getMyAlerts/' + this.userObj.org_id + ':' + this.userObj.first_name + ':' + status, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getUsersByOrgId(): Observable<object> {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    // console.log(this.userObj.org_id);
    // console.log(this.userObj.first_name);
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getUserByOrgId/' + this.userObj.org_id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  ///getloginUser(): Observable<object> {
  // return this.httpClient.get<object>(this.apiURL + '/api/alert/getMyAlerts/2:DAVID BANTON')
  //.pipe(
  //  retry(1),
  //   catchError(this.handleError)
  //)

  // }

  login(_payload) {
    let body = JSON.stringify(_payload);
    console.log(body);
    return this.httpClient.post<any>(this.apiURL + '/api/alert/authenticate', body, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
    //   .pipe(map(resp => {
    //     if (resp) {
    //     // store user details in local storage to keep user logged in
    //     localStorage.setItem('currentUser', JSON.stringify(resp.result));
    //     this.currentUserSubject.next(resp);
    // }
    // return resp;
    // }));
  }

  adacoUpdate(_adacoDetails) {
    let body = JSON.stringify(_adacoDetails);
    console.log(body);
    return this.httpClient.post<any>(this.apiURL + '/api/alert/updateadaco', body, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  multiAdacoUpdate(_adacoDetails) {

    let body = JSON.stringify(_adacoDetails);
    console.log(body);
    return this.httpClient.post<any>(this.apiURL + '/api/alert/updatemultiadaco', body, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  createAlert(createalert: any): Observable<object> {

    let body = JSON.stringify(createalert);
    return this.httpClient.post<object>(this.apiURL + '/api/alert/createAlert', body, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateAlert(updatealert: any): Observable<object> {
    let body = JSON.stringify(updatealert);
    debugger;
    return this.httpClient.post<object>(this.apiURL + '/api/alert/updateAlert', body, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getalertcreatedlist(_id) {
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAlertsByOrgId/' + _id, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getAlertDetails(_id) {
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAlertDetails/' + _id, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  deleteAlert(_id) {
    return this.httpClient.delete<object>(this.apiURL + '/api/alert/deleteAlert/' + _id, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }


  getDepartment(): Observable<object> {
    return this.httpClient.get<object>(this.apiURL + '/api/alert/departments')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  getDepartmentByOrgId(): Observable<object> {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getDeptByOrgId/' + this.userObj.org_id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  getProcessDetails(processDetail: any): Observable<object> {
    let body = JSON.stringify(processDetail);
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAllAlert/' + this.userObj.org_id)
      //return this.httpClient.post<object>(this.apiURL + '/api/alert/getMyDeptAlerts', body, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getMyDeptAlerts(status, dept_id) {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    // this.userObj = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getMyDeptAlerts/' + this.userObj.org_id + ':' + this.userObj.first_name + ':' + dept_id + ':' + status)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }
  getallalert(): Observable<object> {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAllAlert/' + this.userObj.org_id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }
  //this needs to be called whn allalert and old alert button is checked
  getallalertstatus(status): Observable<object> {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAllAlert/' + this.userObj.org_id + ":" + status)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  //this needs to be called whn allalert and close alert button is checked
  getAllAlertByStatus(status): Observable<object> {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAllAlertByStatus/' + this.userObj.org_id + ":" + status)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  //this needs to be called whn allalert and old alert button is checked
  getAllOldAlertByStatus(status): Observable<object> {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAllOldAlerts/' + this.userObj.org_id + ":" + status)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  //this needs to be called whn allalert and old alert button is checked
  getMyOldAlerts(status): Observable<object> {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getMyOldAlerts/' + this.userObj.org_id + ":" + this.userObj.first_name + ":" + status)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  //between dates
  getallAlertByDates(fromdate, todate, status): Observable<object> {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    debugger;
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAllAlertByDates/' + this.userObj.org_id + ':' + fromdate + ':' + todate + ':' + status)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  createAPI(_payload) {
    return this.httpClient.post<object>(this.apiURL + '/api/alert/createApi', _payload, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getAllApis() {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAllApi/' + this.userObj.org_id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  getApiById(_id): Observable<object> {
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getApiDetailsById/' + _id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  updateApi(_payload): Observable<object> {
    return this.httpClient.post<object>(this.apiURL + '/api/alert/updateApi', _payload, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  createCompany(_payload) {
    return this.httpClient.post<object>(this.apiURL + '/api/alert/createCompany', _payload, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getAllCompanies() {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getAllCompany')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  getCompanyById(_id): Observable<object> {
    return this.httpClient.get<object>(this.apiURL + '/api/alert/getCompanyDtlsById/' + _id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  updateCompany(_payload): Observable<object> {
    return this.httpClient.post<object>(this.apiURL + '/api/alert/updateCompany', _payload, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

  }

  getSopDoc(_id){
    return this.httpClient.get<any>(this.apiURL + '/api/alert/getSopDoc/' + _id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  alertExists(orgId, apiId){
    return this.httpClient.get<any>(this.apiURL + '/api/alert/alertExists/' + orgId +':'+apiId, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

}


