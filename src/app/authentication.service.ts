import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { ApiService } from './api.service';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    API_URL: any;
    constructor(private router: Router, private httpClient: HttpClient, private service: ApiService) {
        // if (window.location.origin.lastIndexOf('localhost') !== -1) {
        //    this.API_URL = this.service.apiURL;
        //} else {
        //  this.API_URL = window.location.origin;
        //}
        this.API_URL = this.service.apiURL;
        console.log(this.API_URL);
    }

    login(_userData) {
        return this.httpClient.post<any>(this.API_URL + '/api/alert/authenticate', _userData)
            .pipe(map(user => {
                if (user) {
                    localStorage.removeItem('user');
                    localStorage.setItem('user', JSON.stringify(user));
                }
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('user');
        this.router.navigateByUrl("/login");
    }
}