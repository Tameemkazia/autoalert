import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { retry, catchError, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  userObj: any;
  companyList: any;
  constructor(private spinner: NgxSpinnerService, private router: Router, private restApi: ApiService, private http: HttpClient) { }

  ngOnInit() {
    this.getAllCompanies();
  }

  OnbuttonClick() {
    this.router.navigate(['/layout/companycreation']);
  }

  getAllCompanies(){
    this.spinner.show();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[ 0, "asc" ]],
    };

    this.http.get(this.restApi.apiURL+'/api/alert/getAllCompany')
    .pipe(map(this.extractData))
      .subscribe(_res => {
        this.spinner.hide();
        this.companyList = _res;
        this.dtTrigger.next();
      });

    // this.restApi.getAllCompanies().subscribe(_res=>{
    //   this.companyList = _res['company'];
    // }) 
  }

  
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  private extractData(res: Response) {
    const body = res['company'];
    return body || {};
  }

  removeCompany(_payload){
    this.spinner.show();
    this.userObj = JSON.parse(localStorage.getItem('user'));
    _payload['is_deleted'] = "1";
    _payload['created_By'] = this.userObj.user_id;
    debugger;
    this.restApi.updateCompany(_payload).subscribe(_res => {
      this.router.navigate(['/layout/company']);
      this.spinner.hide();
    })
  } 
}