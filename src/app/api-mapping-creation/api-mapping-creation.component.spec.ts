import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiMappingCreationComponent } from './api-mapping-creation.component';

describe('ApiMappingCreationComponent', () => {
  let component: ApiMappingCreationComponent;
  let fixture: ComponentFixture<ApiMappingCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiMappingCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiMappingCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
