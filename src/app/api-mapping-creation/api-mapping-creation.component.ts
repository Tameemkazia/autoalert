import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ApiService } from '../api.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-api-mapping-creation',
  templateUrl: './api-mapping-creation.component.html',
  styleUrls: ['./api-mapping-creation.component.css']
})
export class ApiMappingCreationComponent implements OnInit {

  apiForm: FormGroup;
  submitted: boolean = false;
  userObj: any;
  paramValue: any;
  editApi: boolean = false;
  editApiId: any;
  ApiDetailsData: any;

  constructor(private spinner: NgxSpinnerService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private restApi: ApiService) {
    this.userObj = JSON.parse(localStorage.getItem('user'));
    this.apiForm = this.formBuilder.group({
      api_name: ['', Validators.required],
      url: ['', Validators.required],
      in_param: [''],
      out_param: [''],
      return_type: [''],
      org_id: [this.userObj.org_id],
      is_deleted:['0']
    })
  }

  ngOnInit() {
    this.route.queryParams.subscribe(queryParams => {
      this.paramValue = queryParams;
      debugger;
      if(this.paramValue.param !== undefined){
        this.editApi = true;
        this.editApiId = this.paramValue.param;
        this.getApiDetails(this.paramValue.param);
      }
  });
  }

  OnbuttonClickClose() {
    this.router.navigate(['/layout/apimapping']);
  }

  getApiDetails(_id){
    this.spinner.show();
    this.restApi.getApiById(_id).subscribe(_res=>{
      this.ApiDetailsData = _res;
      this.spinner.hide();
      this.apiForm.patchValue(_res);
    })
  }

  get f() { return this.apiForm.controls; }

  submit() {
    this.submitted = true;
    if (this.apiForm.invalid) {
      return
    }
    this.spinner.show();

    if(this.editApi === false){
      this.restApi.createAPI(this.apiForm.value).subscribe(_res=>{
        this.router.navigate(['/layout/apimapping']);
        this.spinner.hide();
      })
    }
    else{
      this.userObj = JSON.parse(localStorage.getItem('user'));
      this.apiForm.value.is_deleted = "0";
      this.apiForm.value.created_By = this.userObj.user_id;
      this.apiForm.value.api_Id = this.editApiId;
      debugger;
      this.restApi.updateApi(this.apiForm.value).subscribe(_res=>{
        this.router.navigate(['/layout/apimapping']);
        this.spinner.hide();
    })
    }
    
  }

}
