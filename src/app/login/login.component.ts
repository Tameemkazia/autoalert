import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { HelperService } from '../helper/helper.service';
import { ApiService } from '../api.service';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('DelModal', { static: false }) delModal;
  loginForm: FormGroup;
  submitClick = false;
  submitted = false;
  returnUrl: string;
  loginservice: any;
  userdata: any;
  // EmailID:any;
  // UserPassword:any;
  invalidUser: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private Helper: HelperService,
    public restApi: ApiService,
    private authenticationService: AuthenticationService) { }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    console.log(this.loginForm);
  }
  get formData() {
    return this.loginForm.controls;
  }
  //form submit
  onLogin() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    this.authenticationService.login(this.loginForm.value).subscribe(_res => {
      if (_res.response == 'Success') {
        this.router.navigate(['/layout/user']);
      }
      if(_res.response == 'Invalid'){
        this.invalidUser = true;
      }
      console.log(_res);
    })

    // this.restApi
    //   .login(loginDetails)
    //   .subscribe(resp => {
    //    console.log(resp);
    //   if(resp)
    //   {
    //      this.userdata = resp;
    //      localStorage.setItem('user', JSON.stringify(this.userdata));
    //      JSON.parse(localStorage.getItem('user'));
    //   }
    //   this.router.navigate(['/layout/user']);

    //   });


  };
  //  this.authenticationService.UserLogin(login).subscribe(
  //    (response) => {

  //      this.Helper.setValue('LoginInfo', response);
  //      this.loginservice={

  //        ModuleURL:response.ModuleURL,
  //      ParentMenu:response.ParentMenu,
  //        RoleId:response.RoleId

  //      }
  //        this.Helper.setValue('DefaultLink', this.loginservice);

  //       if(response.Division>0)
  //        {
  //          this.router.navigate([response.ModuleURL]);
  //       }
  //       else{

  //       this.delModal.show();
  //        }
  //    });





}


