import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CandowntimeComponent } from './candowntime/candowntime.component';
import { RouterModule } from '@angular/router';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { InputGrpComponent } from './input-grp/input-grp.component';
import { LayoutComponent } from './layout/layout.component';
// ngx-bootstrap functionality
import { SharedBootstrapModule } from './shared/shared-bootstrap.module';
import { DataTablesModule } from 'angular-datatables';
import { SeamerVaccumCheckComponent } from './forms/seamer-vaccum-check/seamer-vaccum-check.component';

import { HeaderComponent } from './header/header.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {TreeModule} from 'primeng/tree';
import {TreeNode} from 'primeng/api';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ng6-toastr-notifications';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { LoginComponent } from './login/login.component';
//import { BrineTemperatureCheckComponent } from './forms/brine-temperature-check/brine-temperature-check.component';
import { BrineTemperatureComponent } from './forms/brine-temperature/brine-temperature.component';
import { FillerweightspeedComponent } from './forms/fillerweightspeed/fillerweightspeed.component';
import { CheckWeigherLogComponent } from './forms/check-weigher-log/check-weigher-log.component';
import { QAFormComponent } from './forms/qaform/qaform.component';
import { CanningConveyorBeltComponent } from './forms/canning-conveyor-belt/canning-conveyor-belt.component';
import { UserComponent } from './admin/user/user.component';
import { RoleComponent } from './admin/role/role.component';
import { RolepermissionComponent } from './admin/rolepermission/rolepermission.component';
import { PoVerificationComponent } from './forms/po-verification/po-verification.component';
import { IngredientReportComponent } from './forms/ingredient-report/ingredient-report.component';
import { CategoryViewComponent } from './forms/category-view/category-view.component';
import { CanFillerComponent } from './forms/can-filler/can-filler.component';
import { FormDetailsComponent } from './forms/form-details/form-details.component';
import { MarinateComponent } from './forms/marinate/marinate.component';

@NgModule({
  declarations: [   
    AppComponent,
    CandowntimeComponent,
    InputGrpComponent,
    LayoutComponent,
    SeamerVaccumCheckComponent,
    HeaderComponent,
    LoginComponent,
    BrineTemperatureComponent,
    FillerweightspeedComponent,
    CheckWeigherLogComponent,
    QAFormComponent,
    CanningConveyorBeltComponent,
    UserComponent,
    RoleComponent,
    RolepermissionComponent,
    PoVerificationComponent,
    IngredientReportComponent,
    CategoryViewComponent,
    CanFillerComponent,
    FormDetailsComponent,
    MarinateComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TreeModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgMultiSelectDropDownModule,
    DataTablesModule,
    SharedBootstrapModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot([
      {
        path: '',component: LoginComponent
      },
      { path: 'login', component: LoginComponent },
      { path: 'layout', component: LayoutComponent,
     
      children: [
        { path: 'user', component: UserComponent},
        { path: 'role', component: RoleComponent},
        { path: 'rolepermission', component: RolepermissionComponent},
     
       ]
    
      },
      { path: '', component: HeaderComponent,
     
      children: [
       
        { path: 'SeamerVaccumCheck', component: SeamerVaccumCheckComponent},
        { path: 'CanDowntime', component: CandowntimeComponent},
        { path: 'BrineTemperatureCheck', component: BrineTemperatureComponent},
        { path: 'CheckWeigherLog', component: CheckWeigherLogComponent},
        { path: 'Fillerweightspeed', component: FillerweightspeedComponent},
        { path: 'CanningConveyorBelt', component: CanningConveyorBeltComponent},
        { path: 'QAForm', component: QAFormComponent},
        { path: 'PoVerification', component: PoVerificationComponent },
        { path: 'IngredientReport', component: IngredientReportComponent },
        { path: 'CategoryView', component: CategoryViewComponent },
        { path: 'CanFiller', component: CanFillerComponent },
        { path: 'FormDetails', component: FormDetailsComponent },
        { path: 'Marinate', component: MarinateComponent },
      ]
    
      },
      { path: '**', redirectTo: '/login', pathMatch: 'full' },
    ]),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
