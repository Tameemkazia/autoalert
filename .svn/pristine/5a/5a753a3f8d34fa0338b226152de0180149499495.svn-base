import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { Dropdown } from 'src/app/helper/dropdown.validator';
import { NoWhitespaceValidator } from 'src/app/helper/no-whitespace.validator';
import { DataTableDirective } from 'angular-datatables';
import { CheckweigherService } from 'src/app/services/checkweigher.service';
import { Icheckweigher, Icheckweigherdelete, IcheckweigherData, ILine } from 'src/app/interface/icheckweigher';
import { Icandownproduct } from 'src/app/interface/iseamer';
import { ToasterService } from 'src/app/helper/toaster.service';
import { formatDate } from '@angular/common';
import { HelperService } from 'src/app/helper/helper.service';


@Component({
  selector: 'app-check-weigher-log',
  templateUrl: './check-weigher-log.component.html',
  styleUrls: ['./check-weigher-log.component.css']
})
export class CheckWeigherLogComponent implements OnInit {
    weigherForm: FormGroup;
    checkweigherdetails:any;
    getweighernames:any;
    submitClick = false;
    submitted = false;
    deletevalue:any;
    CheckID:any;
    linedetails:any;
    checkweigherID:number;
    CheckweigherId:any;
    canDownDropdown = [];
    checkeditdetails:any;
    WeigherDate:any;
    PopupDate:any;
    ProductDropDown= [];
    maxDate = new Date();
    ddline:number;
    prodtype:number;
    FrmDate:Date;
    ToDate:Date;
    loginInfo:any;
    minDate= new Date();
    LineNo: any;
    CheckweigherID:number;
    previousValues:any;
    SavePrevious:any;
    valuetrial:any;
    savedate:string;
    onchangeProductDate:string;
    FrmDates:string;
    ToDates:string;
    PopupDate1:string;
      // We use this trigger because fetching the list of persons can be quite long,
      // thus we ensure the data is fetched before rendering
      public dtTrigger: Subject<any> = new Subject();
      @ViewChild(DataTableDirective,{static: false}) dtElement: DataTableDirective; 
      @ViewChild('DelModal',{static: false}) DelModal;
      @ViewChild('WeigherModal',{static: false}) myModal;
      reportDate='_'+ formatDate(new Date(), 'dd/MM/yyyy', 'en-US');
      dtOptions:  any = {};
      isMeridian = false;
      readonly = true;
      Time = new Date();

  constructor(private formBuilder: FormBuilder,
     private router: Router,private toaster:ToasterService, private helper:HelperService, private checkweigherservice: CheckweigherService){}

  ngOnInit() {
    this.prodtype=2;
    this.getLines();
    this.FrmDate = new Date();
    this.ToDate = new Date();
    this.LineNo=1;
    this.CheckweigherId=1;
   
   // this.GetCheckWeigherName();
   this.loginInfo = this.helper.getValue('LoginInfo');
   this.FrmDates=formatDate(this.FrmDate.toLocaleDateString(),'yyyy-MM-dd','en-US');
   this.ToDates=formatDate(this.ToDate.toLocaleDateString(),'yyyy-MM-dd','en-US');
   this.dtOptions = {
    dom: 'Blfrtip',
      "paging":   false,
      "ordering": false,
      "info":     false,
    buttons: [
      {
        extend:    'copyHtml5',
        text:      '<i class="fa fa-files-o" style="color:#1d1d85;"></i>',
        titleAttr: 'Copy',
        title: 'Check Weigher Log'+this.reportDate,          
       
        
    },
    {
        extend:    'excelHtml5',
        text:      '<i class="fa fa-file-excel-o" style="color:#1d1d85;"></i>',
        titleAttr: 'Excel',
        title: 'Check Weigher Log'+this.reportDate,          
        
    },
    {
        extend:    'csvHtml5',
        text:      '<i class="fa fa-file-text-o" style="color:#1d1d85;"></i>',
        titleAttr: 'CSV',
        title: 'Check Weigher Log'+this.reportDate,          
        
    },
    {
        extend:    'print',
        text:      '<i class="fa fa-file-pdf-o" style="color:#1d1d85;"></i>',
        titleAttr: 'PDF',
        title: 'Check Weigher Log'+this.reportDate,          
        
    }
       
    ]
  }

    this.weigherForm = this.formBuilder.group({
      LineNo: ['', [Validators.required, Dropdown]],
      CheckDate: ['', [Validators.required]],
      ProductName: ['', [Validators.required, NoWhitespaceValidator.cannotContainSpace]],
      CheckStartTime: ['', [Validators.required]],
      CriticalWTRequired: ['', [Validators.required,Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      ActualWTOfCriticalCan: ['', [Validators.required,Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      StartUpEVALResults1: ['', [Validators.required,Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      LowWTRequired: ['', [Validators.required,Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      ActualWTOfLowCan: ['', [Validators.required,Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      StartUpEVALResults2: ['', [Validators.required,Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      ComponentsBeingCheckWeighed: ['', [Validators.required, NoWhitespaceValidator.cannotContainSpace]],
      Shift:['', [Validators.required, Dropdown]],
      CheckWeigherOperator: ['', [Validators.required, NoWhitespaceValidator.cannotContainSpace]],
     // Initials: ['', [Validators.required, NoWhitespaceValidator.cannotContainSpace]],
      Trial: ['5',[Validators.required]],
      Reject: ['5',[Validators.required]],
      Trial1: ['5',[Validators.required]],
      Reject1: ['5',[Validators.required]],
      Hi_LoRejecter: ['', [Validators.required, Dropdown]],
      RecheckAfterRestart: ['', [Validators.required, Dropdown]],
      RecheckBeforeRestart: ['', [Validators.required, Dropdown]],
      Comments: [''],
      Floor_QA_CheckAndApproval: [''],
      Office_CheckAndApproval: [''],
      Initials2: [''],
      Initials1: [''],
    });


  }
  get weigher() { return this.weigherForm.controls; }

/*------ Modal Pop-Up -----*/
  showModal() {
    this.valuetrial='5';
    this.weigherForm.reset();
    this.CheckID=0;
    this.PopupDate = new Date();
    this.PopupDate1=formatDate(this.PopupDate.toLocaleDateString(),'yyyy-MM-dd','en-US');
    this.weigher.CheckDate.setValue(this.PopupDate1);
    this.weigher.Trial.setValue( this.valuetrial);
    this.weigher.Reject.setValue( this.valuetrial);
    this.weigher.Trial1.setValue( this.valuetrial);
    this.weigher.Reject1.setValue( this.valuetrial);
    const time = new Date();
    time.setHours(time.getHours());
    time.setMinutes(time.getMinutes());
    this.weigher.LineNo.setValue(this.LineNo);

    const time1 = new Date();
    //this.stime=time.setHours(14);
    time1.setHours(time.getHours());
    time1.setMinutes(time.getMinutes());
 
    this.weigher.CheckStartTime.setValue(time1);
    this.myModal.show();
    this.previousValues=this.helper.getValue("CheckWeigher")
    if(this.previousValues.LineNo>=0)
    {
      
      this.weigher.LineNo.setValue(this.previousValues.LineNo);
      this.weigher.CheckWeigherOperator.setValue(this.previousValues.OperatorName);
      this.weigher.Shift.setValue(this.previousValues.Shift);
       this.weigher.CheckDate.setValue(this.previousValues.Date);
       this.getProductDropdown(this.previousValues.Date);
       this.weigher.ProductName.setValue(this.previousValues.ProductCode);
     
     
    }
    else{
    
      this.weigher.LineNo.setValue(this.ddline);
      this.weigher.CheckWeigherOperator.setValue(this.loginInfo.FirstName +" "+this.loginInfo.LastName);
    }
  }
/*------ end ------*/
onChangeDate1(event)
{

  if(event!=null)
  {
  this.ToDates=formatDate(event.toLocaleDateString(),'yyyy-MM-dd','en-US');
  }
  //this.minDate=event;
}
onChangeDate(event)
{
  if(event!=null)
  {
  this.FrmDates=formatDate(event.toLocaleDateString(),'yyyy-MM-dd','en-US');
  }
 
  this.minDate=event;
}

changeLine(changeValue)
    {
     // alert(changeValue);
      this.weigher.Line.setValue(changeValue);

    }


getLines()
{

  const lines: ILine = {

    ProductType: 2
   
  };
  this.checkweigherservice.getLineDetails(lines).subscribe(
    (response) => {
 
      this.linedetails = response;
    
      this.ddline=response[0].Id;
      this.getAllCheckWeigher();
    
      // Calling the DT trigger to manually render the table
     // this.dtTrigger.next();
    },
    error => {
      //this.dtTrigger.next();
     // this.toaster.error("Error while getting line details", "Error!");
    });


}


/*---- Getting Table Data --------*/
GetCheckWeigherName() 
{
  this.checkweigherservice.GetCheckWeigherName().subscribe(
    (response) => {
     //this.DelModal.hide();
     console.log(response);
     this.getweighernames=response;
     this.CheckweigherId=this.getweighernames[0].Id;
      //this.getAllCheckWeigher();
    });
}
getAllCheckWeigher()
{
  
  
  //alert(date);
  const checkweigher: Icheckweigher = {

    FrmDate:this.FrmDates,
      ToDate: this.ToDates, 
    LineNo: this.ddline,
    
    //CheckWeigherid:this.CheckweigherId
  };
console.log(checkweigher,"checkweigher");
  // getall users
  this.checkweigherservice.getAllCheckWeigherData(checkweigher).subscribe(
    (response) => {
console.log(response,"resp");
    

      if(this.dtElement.dtInstance!=null){
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
        });
      }
  
  console.log(response);
      this.checkweigherdetails = response;

      this.dtTrigger.next();
    },
    error => {
      this.dtTrigger.next();
      //this.toaster.error("Error while getting Check Weigher details", "Error!");
    });
}
/*------ End ------*/

/*------ Adding Data ----------*/
onSubmit(weigherFormvalue)
{
  this.helper.removeValue("CheckWeigher");
  this.submitted = true;
  // stop here if form is invalid
  if (this.weigherForm.invalid) {
    return;
  }

  if(weigherFormvalue.Floor_QA_CheckAndApproval==true)
    {
      weigherFormvalue.Floor_QA_CheckAndApproval=1;
    }
    else{
      weigherFormvalue.Floor_QA_CheckAndApproval=0;
    }
    if(weigherFormvalue.Office_CheckAndApproval==true)
    {
      weigherFormvalue.Office_CheckAndApproval=1;
    }
    else{
      weigherFormvalue.Office_CheckAndApproval=0;
    }
    if(weigherFormvalue.CheckDate instanceof Date) {  console.log(true); 
      this.savedate= formatDate(weigherFormvalue.CheckDate.toLocaleDateString(),'yyyy-MM-dd','en-US');
    }
    else
    {
      this.savedate=weigherFormvalue.CheckDate;
    }
  //alert(Seamervalue.Date);
  const checkweigherdata: IcheckweigherData = {
    Id: this.CheckID > 0 ? this.CheckID : 0,
   // Id:this.SeamerID,
   //Date:this.Date.getFullYear() + '-' + ('0' + (this.Date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.Date.getDate()).slice(-2),
  
  // Id:weigherFormvalue.recordedBy,  
   CheckWeigherid:weigherFormvalue.LineNo,   
   CheckDate:this.savedate,   
   CheckStartTime:weigherFormvalue.CheckStartTime.getHours()+":"+weigherFormvalue.CheckStartTime.getMinutes(),  
   ProductName:weigherFormvalue.ProductName,  
   CriticalWTRequired:weigherFormvalue.CriticalWTRequired,  
   ActualWTOfCriticalCan:weigherFormvalue.ActualWTOfCriticalCan,  
   StartUpEVALResults1:weigherFormvalue.StartUpEVALResults1,  
   StartUpEVALResults2:weigherFormvalue.StartUpEVALResults2,  
   LowWTRequired:weigherFormvalue.LowWTRequired,  
   ActualWTOfLowCan:weigherFormvalue.ActualWTOfLowCan,  
   CheckWeigherOperator:weigherFormvalue.CheckWeigherOperator,  
   ComponentsBeingCheckWeighed:weigherFormvalue.ComponentsBeingCheckWeighed,  
   //CheckTime: weigherFormvalue.CheckTime.getHours()+":"+weigherFormvalue.CheckTime.getMinutes(),  
   Initials:' ',  
   Hi_LoRejecter:weigherFormvalue.Hi_LoRejecter,  
   Comments:weigherFormvalue.Comments==null?' ':weigherFormvalue.Comments,  
   RecheckBeforeRestart:weigherFormvalue.RecheckBeforeRestart,    
   RecheckAfterRestart:weigherFormvalue.RecheckAfterRestart,  
   Floor_QA_CheckAndApproval:weigherFormvalue.Floor_QA_CheckAndApproval,  
   Office_CheckAndApproval:weigherFormvalue.Office_CheckAndApproval,  
   Initials1:weigherFormvalue.Initials1==null?' ':weigherFormvalue.Initials1, 
   Initials2:weigherFormvalue.Initials2==null?' ':weigherFormvalue.Initials2, 
   CriticalCanTrial:weigherFormvalue.Trial,  
   LowCanTrial:weigherFormvalue.Trial1,  
   CriticalCanReject:weigherFormvalue.Reject,  
   LowCanReject:weigherFormvalue.Reject1,   
   Shift:weigherFormvalue.Shift,
   CreatedBy:this.loginInfo.UserId
  
  };

  this.SavePrevious={
     
    Shift:weigherFormvalue.Shift,
    OperatorName:weigherFormvalue.CheckWeigherOperator,
    ProductCode:weigherFormvalue.ProductName,
    LineNo:weigherFormvalue.LineNo,
    Date:this.savedate

  }
 
  this.helper.setValue("CheckWeigher", this.SavePrevious);
  this.checkweigherservice.CheckWeigherInsertOrUpdate(checkweigherdata).subscribe(
    (response) => {
     console.log(response);
      this.submitted = false;
     
      this.myModal.hide();
      this.GetCheckWeigherNamesubmit();
      this.toaster.success("Updated Successfully","Success");

      this.FrmDates=this.savedate;
      this.ToDates=this.savedate;
      this.ddline=weigherFormvalue.LineNo;

      this.FrmDate=weigherFormvalue.CheckDate;
      this.ToDate=weigherFormvalue.CheckDate;
      this.getAllCheckWeigher();
    },

    error => {
      //this.toaster.error("Some error occurred.Please try again","Error");
      this.submitted = false;
      this.myModal.hide();
      this.weigherForm.reset();
    
    });
}


onValueChange1(changeDate)
{

  if(changeDate!=null)
  {
  this.getProductDropdown(changeDate);
  }
}
public getProductDropdown(onchangedate) {
    
  if(onchangedate instanceof Date) { 
    //let date=formatDate(onchangedate, 'yyyy-MM-dd', 'en-US','-0500')
    
    this.onchangeProductDate=formatDate(onchangedate.toLocaleDateString(),'yyyy-MM-dd','en-US');
    }
    else{
      this.onchangeProductDate=onchangedate;
    }

  this.ProductDropDown=[];
  const dashboard: Icandownproduct = {
    FrmDate: this.onchangeProductDate,
    ToDate: this.onchangeProductDate,
    
    LineNo:"1",
    ProductType:this.prodtype
   };
  console.log(dashboard,"dashboard");
      this.checkweigherservice.getProduct(dashboard).subscribe(
    (response) => {
      if(response.length>0)
      {
     this.ProductDropDown =response;
    //this.ddlproduct= this.canDownProductDropDown[0].Product_ID
      }
      else
      {
        this.ProductDropDown = [];    
      }
     // console.log( this.canDownProductDropDown);
    });
}
GetCheckWeigherNamesubmit() 
{
  this.checkweigherservice.GetCheckWeigherName().subscribe(
    (response) => {
     //this.DelModal.hide();
     console.log(response);
     this.getweighernames=response;
    // this.CheckweigherId=this.getweighernames[0].Id;
      //this.getAllCheckWeigher();
    });
}
/*----- End --------*/


/*---- Edit Section ------*/
onedit(Id)
  {
    this.CheckID=Id;
    this.Editdetails();
  }
  Editdetails()
  {
    this.myModal.show();
    const checkweigherdelete: Icheckweigherdelete = {

      Id:this.CheckID
      
    };
  this.checkweigherservice.CheckWeigherEdit(checkweigherdelete).subscribe(
    (response) => {
      this.checkeditdetails=response;
      console.log(response);
      const time = new Date();
      //this.stime=time.setHours(14);
      time.setHours(this.checkeditdetails.CheckStartTime.slice(0,2));
      time.setMinutes(this.checkeditdetails.CheckStartTime.slice(3,5));
   
     
       this.weigher.CheckStartTime.setValue(time);
       const time1 = new Date();
       //this.stime=time.setHours(14);
      // time1.setHours(this.checkeditdetails.CheckTime.slice(0,2));
       //time1.setMinutes(this.checkeditdetails.CheckTime.slice(3,5));
    


       //this.weigher.CheckTime.setValue(time1);
       this.weigher.LineNo.setValue(this.checkeditdetails.CheckWeigherid);
       this.weigher.CheckDate.setValue(this.checkeditdetails.CheckDate);
       this.getProductDropdown(this.checkeditdetails.CheckDate.slice(0,10));
       this.weigher.ProductName.setValue(this.checkeditdetails.ProductName);
       this.weigher.CriticalWTRequired.setValue(this.checkeditdetails.CriticalWTRequired);
       this.weigher.ActualWTOfCriticalCan.setValue(this.checkeditdetails.ActualWTOfCriticalCan);
       this.weigher.StartUpEVALResults1.setValue(this.checkeditdetails.StartUpEVALResults1);
       this.weigher.LowWTRequired.setValue(this.checkeditdetails.LowWTRequired);
       this.weigher.ActualWTOfLowCan.setValue(this.checkeditdetails.ActualWTOfLowCan);
       this.weigher.StartUpEVALResults2.setValue(this.checkeditdetails.StartUpEVALResults2);
       this.weigher.CheckWeigherOperator.setValue(this.checkeditdetails.CheckWeigherOperator);
       this.weigher.ComponentsBeingCheckWeighed.setValue(this.checkeditdetails.ComponentsBeingCheckWeighed);
      // this.weigher.Initials.setValue(this.checkeditdetails.Initials);
       this.weigher.Hi_LoRejecter.setValue(this.checkeditdetails.Hi_LoRejecter);
       this.weigher.Trial.setValue(this.checkeditdetails.CriticalCanTrial);
       this.weigher.Reject.setValue(this.checkeditdetails.CriticalCanReject);
       this.weigher.Trial1.setValue(this.checkeditdetails.LowCanTrial);
       this.weigher.Reject1.setValue(this.checkeditdetails.LowCanReject);
       this.weigher.RecheckBeforeRestart.setValue(this.checkeditdetails.RecheckBeforeRestart);
       this.weigher.RecheckAfterRestart.setValue(this.checkeditdetails.RecheckAfterRestart);
       this.weigher.Comments.setValue(this.checkeditdetails.Comments);
       this.weigher.Initials1.setValue(this.checkeditdetails.Initials1);
       this.weigher.Initials2.setValue(this.checkeditdetails.Initials2);
       this.weigher.Floor_QA_CheckAndApproval.setValue(this.checkeditdetails.Floor_QA_CheckAndApproval);
       this.weigher.Office_CheckAndApproval.setValue(this.checkeditdetails.Office_CheckAndApproval);
       this.weigher.Shift.setValue(this.checkeditdetails.Shift);
    });
  }
  /*------ End --------*/


/*------ Delete section -------*/
Deletecheckweigher(value)
{
  this.DelModal.show();
  this.deletevalue=value;
}
DeleteTableDetails() {
  
  const checkweigherdelete: Icheckweigherdelete = {

    Id:this.deletevalue
    
  };

  this.checkweigherservice.CheckWeigherDelete(checkweigherdelete).subscribe(
    (response) => {
     this.DelModal.hide();
     console.log(response);
      this.GetCheckWeigherName();
      this.getAllCheckWeigher();
    });
}
/*------ end --------*/
}
